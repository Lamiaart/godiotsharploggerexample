using Godot;
using System;
using LoggerExample.Loggers;

public class Example : Node2D
{
	public override void _Ready()
	{
		Logger logger = GetNode<Logger>("Logger");

		logger.Log("Starting example", LogType.Init);

		logger.Log("Input example", LogType.Input);

		logger.Log("Generating example", LogType.Generating);

		logger.Log("Action example", LogType.Action);
	}

}
