using System.Collections.Generic;

/**
 * Manages the given loggers
 */
namespace LoggerExample.Loggers
{
    public class MainLogger : ILogger
    {
          private readonly int _logMask;

          private readonly List<ILogger> _loggers;
          private static MainLogger _instance;

          private protected MainLogger(int logMask)
          {
              _logMask = logMask;
              _loggers = new List<ILogger>();
          }

          public static MainLogger GetLogger()
          {
              if (_instance == null)
              {
                  _instance = new MainLogger(0);
              }

              return _instance;
          }

          public void AddLogger(ILogger logger)
          {
              _loggers.Add(logger);
          }
          public void Log(string message, LogType type)
          {
              foreach (ILogger logger in _loggers)
              {
                  logger.Log(message, type);
              }
          }
      }
}