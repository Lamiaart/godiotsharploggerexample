namespace LoggerExample.Loggers
{
    //  Because you set bits you have to only values of 2^x here (so double the previos number basicly)
    public enum LogType
    {
        Init = 1,
        Generating = 2,
        Action = 4,
        Input = 8,
    }
}