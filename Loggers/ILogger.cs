namespace LoggerExample.Loggers
{
    public interface ILogger
    {
        void Log(string message, LogType type);
    }
}