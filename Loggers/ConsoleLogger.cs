using System;

/**
 * Will log to the console, does not show up in godot editor
 */
namespace LoggerExample.Loggers
{
    public class ConsoleLogger : ILogger
    {
        protected readonly int _logMask;
        protected static ILogger _instance;

        public ConsoleLogger(int logMask)
        {
            _logMask = logMask;
        }

        public virtual void Log(string message, LogType type)
        {
            if (shouldLog(type))
            {
                Console.WriteLine($"[{type.ToString()}]:  {message}");
            }
        }


        private bool shouldLog(LogType type)
        {
            return ((int) type & _logMask) > 0;
        }
    }
}