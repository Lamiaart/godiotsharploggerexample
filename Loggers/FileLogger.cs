using System;
using System.IO;
using File = System.IO.File;

namespace LoggerExample.Loggers
{
    public class FileLogger : ILogger
    {
        private readonly int _logMask;
        private StreamWriter writer;

        public FileLogger(int logMask, string filePath)
        {
            _logMask = logMask;
            if (!File.Exists(filePath))
            {
                using (StreamWriter streamWriter = File.CreateText(filePath))
                {
                    streamWriter.WriteLine("Start of log \n");
                    streamWriter.Close();
                }
            }

            writer = File.AppendText(filePath);
        }

        private bool shouldLog(LogType type)
        {
            return ((int) type & _logMask) > 0;
        }

        public void Log(string message, LogType type)
        {

            if (shouldLog(type))
            {
                DateTime date = DateTime.Now;
                writer.WriteLine($"{date.ToString()},{type.ToString()},{message}");
                writer.Flush();
            }
        }
    }
}