using System;
using Godot;

/**
 * Will log to a label in game
 */
namespace LoggerExample.Loggers
{
    public class InGameLogger : ILogger
    {
        private readonly int _logMask;
        private readonly RichTextLabel _label;

        public InGameLogger(int logMask, RichTextLabel label)
        {
            _logMask = logMask;
            _label = label;
        }

        public virtual void Log(string message, LogType type)
        {
            if (shouldLog(type))
            {
                _label.Text = _label.Text + "\n" + message;
            }
        }


        private bool shouldLog(LogType type)
        {
            return ((int) type & _logMask) > 0;
        }
    }
}