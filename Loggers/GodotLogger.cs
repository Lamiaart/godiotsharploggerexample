using System;
using Godot;

/**
 * Will log to godot, also shows up in console
 */
namespace LoggerExample.Loggers
{
    public class GodotLogger : ILogger
    {
        protected readonly int _logMask;

        public GodotLogger(int logMask)
        {
            _logMask = logMask;
        }

        public virtual void Log(string message, LogType type)
        {
            if (shouldLog(type))
            {
                GD.Print($"[{type.ToString()}]:  {message}");
            }
        }


        private bool shouldLog(LogType type)
        {
            return ((int) type & _logMask) > 0;
        }
    }
}