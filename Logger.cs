using Godot;
using LoggerExample.Loggers;

public class Logger : Node
{
	// The items have to be in teh same order here as Godot will make these values  1,2,4,8
	[Export(PropertyHint.Flags, "Init, Generating, Action, Input")] int _logTypeConsole = 0;
	[Export(PropertyHint.Flags, "Init, Generating, Action, Input")] int _logTypeFile = 0;
	[Export(PropertyHint.Flags, "Init, Generating, Action, Input")] int _logTypeInGame = 0;
	[Export(PropertyHint.Flags, "Init, Generating, Action, Input")] int _logTypeEditor = 0;
	[Export] private string _filePath = "/tmp/mylog.log";
	[Export] private string _labelPath = "";
	private MainLogger _logger;
	public override void _Ready()
	{
		// Main Logger is a singeleton. If we ever want to call it from non godot scripts we van use the one that was
		// instanciated by godot
		_logger = MainLogger.GetLogger();

		if (_logTypeConsole != 0)
		{
			_logger.AddLogger(new ConsoleLogger(_logTypeConsole));
		}

		if (_logTypeEditor != 0)
		{
			_logger.AddLogger(new GodotLogger(_logTypeEditor));
		}

		if (_logTypeFile != 0)
		{
			_logger.AddLogger(new FileLogger(_logTypeFile, _filePath));
		}

		if (_logTypeFile != 0)
		{
			_logger.AddLogger(new InGameLogger(_logTypeFile, GetParent().GetNode<RichTextLabel>(_labelPath)));
		}
	}

	public void Log(string message, LogType type)
	{
		_logger.Log(message, type);
	}

}
